
#include "PointLightsManager.h"
#include "Utility.h"
#include <TTH/Resources/Shader.h>

TTH::ComponentRegister<PointLightsManager> PointLightsManager::reg("PointLightsManager");

void PointLightsManager::OnLoad() {
    LightComponent::OnLoad();
}

PointLightsManager *PointLightsManager::Clone() const {
    return nullptr;
}

bool PointLightsManager::LoadFromFile(TTH::IniFile &file) {
    file.SetToSection("PointLightsManager");
    file.RequireValue("name", name);
    file.RequireValue("maxLightsNumber", maxLights);
    lights.reserve(maxLights);
    return true;
}

void PointLightsManager::SetUniforms(TTH::Shader *shader) {
    shader->Use();
    shader->SetInt(name+"_count", lights.size());
    for(auto l : lights){
        std::string n = name+"["+std::to_string(l.num)+"]";
        shader->SetVec3(n+".position", l.position);
        shader->SetVec3(n+".color", l.color / Options::exposure);
    }
}

bool PointLightsManager::CastsShadows() {
    return false;
}
void PointLightsManager::CreateShadowMap() {}

void PointLightsManager::AddLight(glm::vec3 position, glm::vec3 color) {
    if(lights.size() < maxLights) {
        lights.emplace_back(lights.size(), position, color);
    }
}