
#include <TTH/Graphics/Gfx.h>
#include "DirectionalLight.h"
#include "Utility.h"
#include <TTH/Resources/ResourceManager.h>
#include <TTH/Core/ComponentFactory.h>
#include <TTH/Graphics/ShadowMapper.h>
#include <TTH/Core/ObjectManager.h>

TTH::ComponentRegister<DirectionalLight> DirectionalLight::reg("DirectionalLight");

DirectionalLight *DirectionalLight::Clone() const {
    return nullptr;
}

bool DirectionalLight::LoadFromFile(TTH::IniFile &file) {
    file.SetToSection("DirectionalLight");

    file.RequireValue("name", name);
    file.RequireValue("direction", direction);
    file.GetValue("color", color, glm::vec3(1.0f));

    camera = TTH::ObjectManager::GetObject("SceneCamera")->GetComponent<TTH::CameraComponent>();
    return true;
}

void DirectionalLight::SetUniforms(TTH::Shader *shader) {
    shader->Use();
    shader->SetVec3(name+".direction", -parent->transform.position);
    shader->SetVec3(name+".color", color);
    shadowMap.UpdateShader(shader, name, 8);
}

bool DirectionalLight::CastsShadows() {
    return true;
}

void DirectionalLight::CreateShadowMap() {

    glm::mat4 lightProjection, lightView;
    glm::mat4 lightSpaceMatrix;
    float near_plane = 1.0f, far_plane = 100.0f;
    lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
    lightView = glm::lookAt(parent->transform.position, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
    lightSpaceMatrix = lightProjection * lightView;

    TTH::ShadowMapper::FillShadowMap(shadowMap, lightSpaceMatrix);
}

void DirectionalLight::OnLoad() {
    LightComponent::OnLoad();
    shadowMap = TTH::ShadowMapper::CreateNewShadowMap();
    parent->transform.position = glm::normalize(-direction) * 40.0f;
    isDay = true;
}

void DirectionalLight::SetDay() {
    if(!isDay) {
        isDay = true;
        color = glm::vec3(1.0f);
        camera->backgroundColor = glm::vec3(0.4f,0.4f,0.6f);
        Options::SetExposure(1.0f);
    }
}

void DirectionalLight::SetNight() {
    if(isDay){
        isDay = false;
        color = glm::vec3(0.0f);
        camera->backgroundColor = glm::vec3(0.02f, 0.02f, 0.04f);
        Options::SetExposure(5.0f);
    }
}