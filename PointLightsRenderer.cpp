
#include <TTH/Core/ComponentFactory.h>
#include "PointLightsRenderer.h"
#include <TTH/Resources/ResourceManager.h>

TTH::ComponentRegister<PointLightsRenderer> PointLightsRenderer::reg("PointLightsRenderer");

PointLightsRenderer *PointLightsRenderer::Clone() const {
    return nullptr;
}

void PointLightsRenderer::Draw() {
    for(auto l : lightsManager->lights){
        shader->Use();
        TTH::Transform t = {l.position, glm::vec3(0.0f), glm::vec3(0.1f), glm::vec3(0.0f)};
        shader->SetMat4("model", t.GetModelMatrix());
        shader->SetVec3("colorTint", l.color);
        model->Draw(*shader);
    }
}

bool PointLightsRenderer::LoadFromFile(TTH::IniFile &file) {
    file.SetToSection("PointLightsRenderer");

    std::string modelName, shaderName;

    file.RequireValue("model", modelName);
    model = TTH::ResourceManager::GetModel(modelName);

    file.RequireValue("shader", shaderName);
    shader = TTH::ResourceManager::GetShader(shaderName);

    return true;
}

void PointLightsRenderer::OnLoad() {
    TTH::RenderComponent::OnLoad();
    lightsManager = parent->GetComponent<PointLightsManager>();
}
