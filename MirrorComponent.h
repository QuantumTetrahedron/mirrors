
#ifndef MIRRORS_MIRRORCOMPONENT_H
#define MIRRORS_MIRRORCOMPONENT_H

#include <TTH/Graphics/RenderComponent.h>
#include <TTH/Postprocessing/Framebuffer.h>
#include <TTH/Resources/ResourceManager.h>
#include <stack>
#include <TTH/Core/CameraComponent.h>
#include "Utility.h"

class MirrorComponent : public TTH::RenderComponent{
public:
    struct MirrorData{
        MirrorComponent* mirror;
        glm::mat4 matrix;
        glm::vec3 position;
        glm::vec3 normal;

        Frustum frustum;

        TTH::Framebuffer* framebuffer;
    };

    MirrorComponent *Clone() const override;

    void SetMaxDepth(int new_depth);

protected:
    glm::mat4 getMirrorMatrix(glm::vec3 normal);
    glm::vec3 Normal;
    Rect rect;

    bool isOneWayMirror;

    void OnLoad() override;

    void Draw() override;

    bool LoadFromFile(TTH::IniFile &file) override;

    static int maxDepth;
    static unsigned int depth;
    static std::deque<MirrorData> mirrorStack;

    static std::vector<TTH::Framebuffer> reflection_framebuffers;
    static std::vector<TTH::Framebuffer> refraction_framebuffers;

    static TTH::UniformBuffer* ubo;
    static bool initialized;

    static TTH::CameraComponent* camera;

private:

    static TTH::ComponentRegister<MirrorComponent> reg;

};

#endif //MIRRORS_MIRRORCOMPONENT_H
