#include <TTH/Input/Input.h>
#include <TTH/Resources/ResourceManager.h>
#include "CameraController.h"
#include "Utility.h"

TTH::ComponentRegister<CameraController> CameraController::reg("CameraController");

void CameraController::Start() {
    cam = parent->GetComponent<TTH::CameraComponent>();

    //TTH::ResourceManager::CreateUniformBuffer("Exposure", sizeof(float));
    /*exposureBuffer = TTH::ResourceManager::GetUniformBuffer("Exposure");

    dayExposure = 1.0f;
    nightExposure = 5.0f;
    day = true;
    exposure = dayExposure;
    exposureBuffer->SetFloat(exposure, 0);*/

    float yaw = parent->transform.rotation.y;
    float pitch = parent->transform.rotation.x;

    glm::vec3 front;
    float p = glm::radians(pitch);
    float y = glm::radians(yaw);
    front.x = glm::cos(p) * glm::cos(y);
    front.y = glm::sin(p);
    front.z = glm::cos(p) * glm::sin(y);
    front = glm::normalize(front);
    cam->SetFront(front);
}

void CameraController::Update(float dt) {
    if(Options::controlsEnabled) {
        glm::vec2 offset = TTH::Input::GetMouseOffset();
        float &yaw = parent->transform.rotation.y;
        float &pitch = parent->transform.rotation.x;

        yaw += offset.x * mouseSensitivity;
        pitch += offset.y * mouseSensitivity;

        if (pitch > 89.0f) {
            pitch = 89.0f;
        } else if (pitch < -89.0f) {
            pitch = -89.0f;
        }

        yaw = glm::mod(yaw, 360.0f);

        glm::vec3 front;
        float p = glm::radians(pitch);
        float y = glm::radians(yaw);
        front.x = glm::cos(p) * glm::cos(y);
        front.y = glm::sin(p);
        front.z = glm::cos(p) * glm::sin(y);
        front = glm::normalize(front);
        cam->SetFront(front);

        glm::vec3 right = glm::cross(front, glm::vec3(0.0f, 1.0f, 0.0f));
        right = glm::normalize(right);

        glm::vec3 up = glm::cross(right, front);
        up = glm::normalize(up);

        float forwardMove = TTH::Input::GetAxis("Vertical");
        float rightMove = TTH::Input::GetAxis("Horizontal");

        parent->transform.position += forwardMove * front * dt * speed;
        parent->transform.position += rightMove * right * dt * speed;
    }
}

void CameraController::OnLeave() {

}

CameraController *CameraController::Clone() const {
    auto ret = new CameraController();
    ret->speed = speed;
    ret->mouseSensitivity = mouseSensitivity;
    return ret;
}

bool CameraController::LoadFromFile(TTH::IniFile &file) {
    file.SetToSection("CameraController");
    if(!file.GetValue("speed", speed)){
        std::cerr << "Missing speed parameter from CameraController" << std::endl;
        return false;
    }

    if(!file.GetValue("mouseSensitivity", mouseSensitivity)){
        std::cerr << "Missing mouseSensitivity parameter from CameraController" << std::endl;
        return false;
    }

    return true;
}
