
#include <TTH/Core/Engine.h>
#include <TTH/Resources/ResourceManager.h>

int main() {

    TTH::Game::Initialize("GameData/EngineOptions.ini");

    TTH::ResourceManager::CreateUniformBuffer("Mirror", sizeof(glm::mat4)+2*sizeof(glm::vec4));

    auto ubo = TTH::ResourceManager::GetUniformBuffer("Mirror");
    ubo->SetMat4(glm::mat4(1.0f), 0);
    ubo->SetVec3(glm::vec3(0.0f), sizeof(glm::mat4));
    ubo->SetVec3(glm::vec3(0.0f), sizeof(glm::mat4)+sizeof(glm::vec4));

    TTH::ResourceManager::CreateUniformBuffer("Exposure", sizeof(float));
    ubo = TTH::ResourceManager::GetUniformBuffer("Exposure");
    ubo->SetFloat(1.0f,0);

    TTH::Game::MainLoop();
    TTH::Game::Shutdown();

    return 0;
}