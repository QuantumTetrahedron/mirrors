
#ifndef MIRRORS_GUIMANAGER_H
#define MIRRORS_GUIMANAGER_H


#include <TTH/UI/HUDComponent.h>
#include <TTH/Core/ComponentFactory.h>
#include <TTH/Core/CameraComponent.h>
#include <TTH/Postprocessing/Framebuffer.h>
#include "PointLightsManager.h"

class GUIManager : public TTH::HUDComponent{
public:
    void Draw() override;

private:
    ~GUIManager() override;

    void OnLoad() override;

    bool LoadFromFile(TTH::IniFile &file) override;

    GUIManager *Clone() const override;

    TTH::Framebuffer fb;
    TTH::CameraComponent* worldCamera;

    std::vector<TTH::Object*> hierarchy;

    void SceneWindow();
    void Inspector();
    void BottomBar();

    void AddMirrorModal();
    void AddOneWayMirrorModal();
    void AddPointLightModal();
    void ImportModal();

    void ObjectInitialConfig();

    //float menuHeight;
    int flags;

    char name[14];
    bool nameExists, nameEmpty;
    TTH::Transform newObjectTransform;


    PointLightsManager* plm;

    static TTH::ComponentRegister<GUIManager> reg;
};



#endif //MIRRORS_GUIMANAGER_H
