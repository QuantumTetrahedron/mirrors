
#ifndef MIRRORS_POINTLIGHTSMANAGER_H
#define MIRRORS_POINTLIGHTSMANAGER_H


#include <TTH/Graphics/LightComponent.h>
#include <TTH/Core/ComponentFactory.h>

class PointLightsManager : public TTH::LightComponent{
public:
    PointLightsManager *Clone() const override;

    bool LoadFromFile(TTH::IniFile &file) override;

    void SetUniforms(TTH::Shader *shader) override;

    bool CastsShadows() override;

    void CreateShadowMap() override;

    void AddLight(glm::vec3 position, glm::vec3 color);

protected:
    void OnLoad() override;

public:

    struct L{
        L(int n, glm::vec3 p, glm::vec3 c)
                : num(n), position(p), color(c){}
        int num;
        glm::vec3 position;
        glm::vec3 color;
    };

    std::vector<L> lights;
private:
    std::string name;
    int maxLights;

    static TTH::ComponentRegister<PointLightsManager> reg;
};



#endif //MIRRORS_POINTLIGHTSMANAGER_H
