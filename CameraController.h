
#ifndef MIRRORS_CAMERACONTROLLER_H
#define MIRRORS_CAMERACONTROLLER_H

#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Core/CameraComponent.h>
#include <TTH/Resources/UniformBuffer.h>

class CameraController : public TTH::BehaviourComponent{
public:
    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;

    CameraController* Clone() const override;

private:
    bool LoadFromFile(TTH::IniFile& file) override;

    TTH::CameraComponent* cam;

    float speed;
    float mouseSensitivity;

    static TTH::ComponentRegister<CameraController> reg;

    //float exposure;
    //float dayExposure;
    //float nightExposure;
    bool day;
    //TTH::UniformBuffer* exposureBuffer;
};

#endif //MIRRORS_CAMERACONTROLLER_H
