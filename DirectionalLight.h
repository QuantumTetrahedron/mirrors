
#ifndef MIRRORS_DIRECTIONALLIGHT_H
#define MIRRORS_DIRECTIONALLIGHT_H


#include <TTH/Graphics/LightComponent.h>
#include <TTH/Graphics/ShadowMapper.h>

class DirectionalLight : public TTH::LightComponent{

public:
    DirectionalLight *Clone() const override;

    bool CastsShadows() override;

    void CreateShadowMap() override;

    void SetUniforms(TTH::Shader *shader) override;

    glm::vec3 direction;
    glm::vec3 color;
    std::string name;

    void SetDay();
    void SetNight();

    bool isDay;
protected:
    bool LoadFromFile(TTH::IniFile &file) override;

    void OnLoad() override;

    TTH::CameraComponent* camera;
private:
    TTH::ShadowMapper::ShadowMap shadowMap;

    static TTH::ComponentRegister<DirectionalLight> reg;
};


#endif //MIRRORS_DIRECTIONALLIGHT_H
