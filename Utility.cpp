
#include <TTH/Resources/ResourceManager.h>
#include "Utility.h"

void Plane::CalcPlane(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2) {
    normal = glm::normalize(glm::cross(p1-p0, p2-p0));
    d = glm::dot(normal, p0);
}

bool Plane::RectInsidePlane(Rect rect) const {
    bool lt_in = glm::dot(rect.topLeft, normal) - d > 0;
    bool rt_in = glm::dot(rect.topRight, normal) - d > 0;
    bool lb_in = glm::dot(rect.bottomLeft, normal) - d > 0;
    bool rb_in = glm::dot(rect.bottomRight, normal) - d > 0;
    return lt_in || rt_in || lb_in || rb_in;
}

void Frustum::CalcPlanes(TTH::CameraComponent* c, Rect rect) {
    glm::vec3 origin = c->GetPosition();

    planes[0].CalcPlane(origin, rect.bottomLeft, rect.topLeft); //left
    planes[1].CalcPlane(origin, rect.topRight, rect.bottomRight); //right
    planes[2].CalcPlane(origin, rect.topLeft, rect.topRight); //top
    planes[3].CalcPlane(origin, rect.bottomRight, rect.bottomLeft); //bottom

    planes[4].CalcPlane(rect.topLeft, rect.topRight, rect.bottomRight); // near

    glm::vec3 relativeCamPos = c->GetPosition() - rect.topLeft; // Any point on the rect plane is fine
    glm::vec3 nearNormal = planes[4].normal;
    float dot = glm::dot(relativeCamPos, nearNormal);
    if(dot > 0){
        for(auto& plane : planes){
            plane.normal *= -1;
            plane.d *= -1;
        }
    }
}

bool Frustum::Contains(Rect r) const {

    // normal case
    bool p0 = planes[0].RectInsidePlane(r);
    bool p1 = planes[1].RectInsidePlane(r);
    bool p2 = planes[2].RectInsidePlane(r);
    bool p3 = planes[3].RectInsidePlane(r);

    bool p4 = planes[4].RectInsidePlane(r);

    return (p0 && p1 && p2 && p3 && p4);
}

Frustum Frustum::fromCamera(TTH::CameraComponent *c) {
    auto proj = c->GetProjectionMatrix();
    proj = proj * c->GetViewMatrix();
    float left[4], right[4], top[4], bottom[4], near[4];//, far[4];
    for(int i = 0; i<4; i++){
        left[i] = proj[i][3] + proj[i][0];
        right[i] = proj[i][3] - proj[i][0];
        bottom[i] = proj[i][3] + proj[i][1];
        top[i] = proj[i][3] - proj[i][1];
        near[i] = proj[i][3] + proj[i][2];
    }
    Frustum f;
    f.planes[0] = {glm::vec3(left[0],left[1],left[2]), -left[3]};
    f.planes[1] = {glm::vec3(right[0], right[1], right[2]), -right[3]};
    f.planes[2] = {glm::vec3(top[0], top[1], top[2]), -top[3]};
    f.planes[3] = {glm::vec3(bottom[0], bottom[1], bottom[2]), -bottom[3]};
    f.planes[4] = {glm::vec3(near[0], near[1], near[2]), -near[3]};
    return f;
}

Rect Rect::Multiply(glm::mat4 matrix) {
    Rect ret;
    ret.topLeft = glm::vec3(matrix * glm::vec4(this->topLeft,1.0f));
    ret.topRight = glm::vec3(matrix * glm::vec4(this->topRight,1.0f));
    ret.bottomLeft = glm::vec3(matrix * glm::vec4(this->bottomLeft,1.0f));
    ret.bottomRight = glm::vec3(matrix * glm::vec4(this->bottomRight,1.0f));
    return ret;
}

int Options::mirrorDepth = 5;
bool Options::controlsEnabled = false;
float Options::exposure = 1.0f;
int Options::sceneDraws = 0;
void Options::SetExposure(float newExposure) {
    exposure = newExposure;
    TTH::ResourceManager::GetUniformBuffer("Exposure")->SetFloat(exposure, 0);
}