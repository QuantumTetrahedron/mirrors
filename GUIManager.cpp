
#include <ImGUI/imgui.h>
#include <TTH/Core/Game.h>
#include "GUIManager.h"
#include "PointLightsManager.h"
#include "DirectionalLight.h"
#include "Utility.h"
#include <ImGUI/imgui_impl_glfw.h>
#include <ImGUI/imgui_impl_opengl3.h>
#include <TTH/Graphics/Gfx.h>
#include <TTH/Core/ObjectManager.h>
#include <TTH/Resources/ResourceManager.h>
#include <TTH/Input/Input.h>
#include <TTH/Core/ObjectFactory.h>
#include <ImGUI/imgui_internal.h>
#include <TTH/Postprocessing/Postprocessing.h>

TTH::ComponentRegister<GUIManager> GUIManager::reg("GUIManager");

void GUIManager::Draw() {

    auto f = TTH::Gfx::GetUsedFramebuffer();
    TTH::Postprocessing::ProcessTo(*worldCamera->framebuffer, fb,"tonemapping");
    TTH::Gfx::UseFramebuffer(f);

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);

    Inspector();

    BottomBar();

    SceneWindow();

    ImGui::PopStyleVar(1);
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

GUIManager *GUIManager::Clone() const {
    return nullptr;
}

bool GUIManager::LoadFromFile(TTH::IniFile &file) {
    return true;
}

void GUIManager::OnLoad() {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(TTH::Game::window.window, true);
    const char* glsl_version = "#version 330 core";
    ImGui_ImplOpenGL3_Init(glsl_version);
    TTH::Gfx::RegisterComponent(this);
    shader = nullptr;
    worldCamera = TTH::ObjectManager::GetObject("SceneCamera")->GetComponent<TTH::CameraComponent>();
    fb.Generate(800,600,false);

    hierarchy = TTH::ObjectManager::GetChildrenOf(TTH::ObjectManager::GetObject("SceneRoot"));
    flags = ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoDecoration;
    newObjectTransform = {glm::vec3(0.0f), glm::vec3(0.0f), glm::vec3(1.0f), glm::vec3(0.0f)};
    TTH::Object* obj = TTH::ObjectManager::GetObject("PointLights");
    plm = obj->GetComponent<PointLightsManager>();
}

GUIManager::~GUIManager() {
    TTH::Gfx::UnregisterComponent(this);
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

void GUIManager::SceneWindow() {
    bool open = true;
    ImGui::SetNextWindowPos(ImVec2(0,0), ImGuiCond_Always, ImVec2(0,0));
    ImGui::SetNextWindowContentSize(ImVec2(800,600));

    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0);
    ImGui::Begin("Scene Window", &open, flags);
    ImVec2 pos = ImGui::GetCursorScreenPos();


    ImGui::GetWindowDrawList()->AddImage((void*)(intptr_t)fb.getTexture()->id, pos,ImVec2(pos.x+800,pos.y+600),ImVec2(0, 1), ImVec2(1, 0));

    if(!Options::controlsEnabled) {
        if (ImGui::IsWindowHovered()) {
            if (ImGui::IsMouseClicked(1)) {
                TTH::Input::DisableCursor();
                Options::controlsEnabled = true;
                ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NoMouse;
            }
        }
    } else {
        if (ImGui::IsMouseReleased(1)) {
            TTH::Input::EnableCursor();
            Options::controlsEnabled = false;
            ImGui::GetIO().ConfigFlags &= ~ImGuiConfigFlags_NoMouse;
        }
    }

    ImGui::End();
    ImGui::PopStyleVar(2);
}

void GUIManager::Inspector() {
    ImGui::SetNextWindowPos(ImVec2(800,0), ImGuiCond_Always, ImVec2(0,0));
    ImGui::SetNextWindowSize(ImVec2(224,600));

    bool open = true;
    ImGui::Begin("Inspector", &open, flags);

    ImGui::Text("FPS: %d",TTH::Game::getFPS());
    ImGui::SameLine();
    ImGui::Text("Scene draws: %d", Options::sceneDraws);
    Options::sceneDraws = 0;

    ImGui::Text("Mirror depth:");

    ImGui::InputInt("",&Options::mirrorDepth,1,1);
    if(Options::mirrorDepth < 1) Options::mirrorDepth = 1;

    ImGui::Separator();

    static bool showingObjects = true;
    if(ImGui::Button("Objects")){
        showingObjects = true;
    }
    ImGui::SameLine();
    if(ImGui::Button("Lights")){
        showingObjects = false;
    }

    if(showingObjects) {
        ImGui::BeginChild("Hierarchy", ImVec2(0, 150), true);
        static TTH::Object *selected = nullptr;
        for (auto obj : hierarchy) {
            bool checked = obj->IsActive();
            ImGui::Checkbox(("##" + obj->GetName()).c_str(), &checked);
            if (checked != obj->IsActive()) obj->SetActive(checked);
            ImGui::SameLine();
            if (ImGui::Selectable(obj->GetName().c_str(), selected == obj))
                selected = obj;
        }
        ImGui::EndChild();
        ImGui::BeginChild("Details");
        if (selected) {
            ImGui::Text("%s", selected->GetName().c_str());

            ImGui::PushItemWidth(60);
            ImGui::Text("Position");
            ImGui::DragFloat("##posx", &selected->transform.position.x, 0.1f);
            ImGui::SameLine();
            ImGui::DragFloat("##posy", &selected->transform.position.y, 0.1f);
            ImGui::SameLine();
            ImGui::DragFloat("##posz", &selected->transform.position.z, 0.1f);
            ImGui::Text("Rotation");
            ImGui::DragFloat("##rotx", &selected->transform.rotation.x, 0.1f);
            ImGui::SameLine();
            ImGui::DragFloat("##roty", &selected->transform.rotation.y, 0.1f);
            ImGui::SameLine();
            ImGui::DragFloat("##rotz", &selected->transform.rotation.z, 0.1f);
            ImGui::Text("Scale");
            ImGui::DragFloat("##scx", &selected->transform.scale.x, 0.1f);
            ImGui::SameLine();
            ImGui::DragFloat("##scy", &selected->transform.scale.y, 0.1f);
            ImGui::SameLine();
            ImGui::DragFloat("##scz", &selected->transform.scale.z, 0.1f);
            ImGui::PopItemWidth();
        }
        ImGui::EndChild();
    } else {
        ImGui::BeginChild("Lights", ImVec2(0, 150), true);
        static float col[3];
        static int selectedLight = -2;

        if(ImGui::Selectable("GlobalLight", selectedLight == -1)){
            selectedLight = -1;
        }

        for (int i = 0; i < plm->lights.size(); i++) {
            if (ImGui::Selectable(("Light"+std::to_string(i+1)).c_str(), selectedLight == i)) {
                selectedLight = i;
                col[0] = plm->lights[selectedLight].color.x;
                col[1] = plm->lights[selectedLight].color.y;
                col[2] = plm->lights[selectedLight].color.z;
            }
        }
        ImGui::EndChild();

        ImGui::BeginChild("LightDetails");
        if (selectedLight > -1) {
            ImGui::Text("Light%d", selectedLight+1);

            ImGui::PushItemWidth(60);
            ImGui::Text("Position");
            ImGui::DragFloat("##lposx", &plm->lights[selectedLight].position.x, 0.1f);
            ImGui::SameLine();
            ImGui::DragFloat("##lposy", &plm->lights[selectedLight].position.y, 0.1f);
            ImGui::SameLine();
            ImGui::DragFloat("##lposz", &plm->lights[selectedLight].position.z, 0.1f);

            ImGui::PopItemWidth();
            ImGui::Text("Color");
            if(ImGui::ColorPicker3("##lcol", col)){
                plm->lights[selectedLight].color = glm::vec3(col[0],col[1],col[2]);
            }
        } else if(selectedLight == -1){
            static DirectionalLight* l = TTH::ObjectManager::GetObject("GlobalLight")->GetComponent<DirectionalLight>();
            if(ImGui::Button("Set Day")){
                l->SetDay();
            }
            ImGui::SameLine();
            if(ImGui::Button("Set Night")){
                l->SetNight();
            }
        }
        ImGui::EndChild();
    }

    ImGui::End();
}

void GUIManager::BottomBar() {
    bool open = true;
    ImGui::SetNextWindowPos(ImVec2(0,600), ImGuiCond_Always, ImVec2(0,0));
    ImGui::SetNextWindowSize(ImVec2(1024,168), ImGuiCond_Always);
    ImGui::Begin("BottomBar", &open, flags);

    if(ImGui::Button("Import model", ImVec2(200,152))){
        strcpy(name, "");
        ImGui::OpenPopup("Import Model");
    }
    ImGui::SameLine();
    if(ImGui::Button("Add mirror", ImVec2(200,152))){
        strcpy(name, "");
        ImGui::OpenPopup("Add Mirror");
    }
    ImGui::SameLine();
    if(ImGui::Button("Add one-way mirror", ImVec2(200,152))){
        strcpy(name, "");
        ImGui::OpenPopup("Add One-way Mirror");
    }
    ImGui::SameLine();
    if(ImGui::Button("Add point light", ImVec2(200,152))){
        strcpy(name, "");
        ImGui::OpenPopup("Add Point Light");
    }
    ImportModal();
    AddMirrorModal();
    AddOneWayMirrorModal();
    AddPointLightModal();

    ImGui::End();
}

void GUIManager::ObjectInitialConfig() {
    if (ImGui::InputText("##objName", name, 14)) {
        nameExists = false;
        for(auto obj : hierarchy){
            if(strcmp(name, obj->GetName().c_str()) == 0){
                nameExists = true;
                break;
            }
        }
    }
    nameEmpty = strcmp(name, "") == 0;
    if(nameExists) {
        ImGui::SameLine();
        ImGui::Text("An object with that name already exists");
    }
    ImGui::Text("Position");
    ImGui::DragFloat("##nposx", &newObjectTransform.position.x,0.1f);
    ImGui::SameLine();
    ImGui::DragFloat("##nposy", &newObjectTransform.position.y, 0.1f);
    ImGui::SameLine();
    ImGui::DragFloat("##nposz", &newObjectTransform.position.z,0.1f);

    ImGui::Text("Rotation");
    ImGui::DragFloat("##nrotx", &newObjectTransform.rotation.x, 0.1f);
    ImGui::SameLine();
    ImGui::DragFloat("##nroty", &newObjectTransform.rotation.y, 0.1f);
    ImGui::SameLine();
    ImGui::DragFloat("##nrotz", &newObjectTransform.rotation.z, 0.1f);

    ImGui::Text("Scale");
    ImGui::DragFloat("##nscx", &newObjectTransform.scale.x, 0.1f);
    ImGui::SameLine();
    ImGui::DragFloat("##nscy", &newObjectTransform.scale.y, 0.1f);
    ImGui::SameLine();
    ImGui::DragFloat("##nscz", &newObjectTransform.scale.z, 0.1f);
}

void GUIManager::AddMirrorModal() {
    if(ImGui::BeginPopupModal("Add Mirror", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {

        ImGui::Text("Adding a mirror.\n\n");
        ImGui::Separator();

        ObjectInitialConfig();

        bool ok = !nameExists && !nameEmpty;
        if (!ok){

            ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
        }
        if (ImGui::Button("Add", ImVec2(120, 0))) {
            TTH::IniFile file;
            file.ReadFile("Archetypes/Mirror.ini");
            auto obj = TTH::ObjectFactory::Create(file, name);
            obj->SetParent(TTH::ObjectManager::GetObject("SceneRoot"));
            obj->transform = newObjectTransform;
            newObjectTransform = {glm::vec3(0.0f), glm::vec3(0.0f), glm::vec3(1.0f), glm::vec3(0.0f)};
            TTH::ObjectManager::AddObject(obj);
            hierarchy.push_back(obj);
            ImGui::CloseCurrentPopup();
        }

        if(!ok){
            ImGui::PopStyleVar();
            ImGui::PopItemFlag();
        }

        ImGui::SetItemDefaultFocus();
        ImGui::SameLine();
        if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
        ImGui::EndPopup();
    }
}

void GUIManager::AddOneWayMirrorModal() {
    if(ImGui::BeginPopupModal("Add One-way Mirror", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {

        ImGui::Text("Adding a one-way mirror.\n\n");
        ImGui::Separator();

        ObjectInitialConfig();

        bool ok = !nameExists && !nameEmpty;
        if (!ok){
            ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
        }
        if (ImGui::Button("Add", ImVec2(120, 0))) {
            TTH::IniFile file;
            file.ReadFile("Archetypes/OneWayMirror.ini");
            auto obj = TTH::ObjectFactory::Create(file, name);
            obj->SetParent(TTH::ObjectManager::GetObject("SceneRoot"));
            obj->transform = newObjectTransform;
            newObjectTransform = {glm::vec3(0.0f), glm::vec3(0.0f), glm::vec3(1.0f), glm::vec3(0.0f)};
            TTH::ObjectManager::AddObject(obj);
            hierarchy.push_back(obj);
            ImGui::CloseCurrentPopup();
        }

        if(!ok){
            ImGui::PopStyleVar();
            ImGui::PopItemFlag();
        }

        ImGui::SetItemDefaultFocus();
        ImGui::SameLine();
        if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
        ImGui::EndPopup();
    }
}

void GUIManager::AddPointLightModal() {
    if(ImGui::BeginPopupModal("Add Point Light", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {

        ImGui::Text("Adding a point light.\n\n");
        ImGui::Separator();

        ImGui::Text("Position");
        ImGui::PushItemWidth(100);
        ImGui::DragFloat("##nposx", &newObjectTransform.position.x,0.1f);
        ImGui::SameLine();
        ImGui::DragFloat("##nposy", &newObjectTransform.position.y, 0.1f);
        ImGui::SameLine();
        ImGui::DragFloat("##nposz", &newObjectTransform.position.z,0.1f);
        ImGui::PopItemWidth();

        static auto color = glm::vec3(1.0f);
        ImGui::Text("Color");
        static float col[3];
        if(ImGui::ColorPicker3("#col", col)) {
            color.x = col[0];
            color.y = col[1];
            color.z = col[2];
        }

        if (ImGui::Button("Add", ImVec2(120, 0))) {
            plm->AddLight(newObjectTransform.position, color);
            color = glm::vec3(1.0f);
            newObjectTransform.position = glm::vec3(0.0f);

            ImGui::CloseCurrentPopup();
        }

        ImGui::SetItemDefaultFocus();
        ImGui::SameLine();
        if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
        ImGui::EndPopup();
    }
}

void GUIManager::ImportModal() {
    if(ImGui::BeginPopupModal("Import Model", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {

        ImGui::Text("Importing a model.\n\n");
        ImGui::Separator();

        ObjectInitialConfig();

        ImGui::Text("Model name (a model named X must have a file Models/X/X.obj):");
        static char buf[256];
        ImGui::InputText("##modelName",buf,256);

        bool ok = !nameExists && !nameEmpty;
        if (!ok){

            ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
        }

        if (ImGui::Button("Add", ImVec2(120, 0))) {

            if(!TTH::ResourceManager::HasModel(buf)){
                std::string filePath = "Models/"+std::string(buf)+"/"+std::string(buf)+".obj";
                TTH::ResourceManager::LoadModel(filePath.c_str(), buf);
            }

            TTH::IniFile file;
            file.ReadFile("Archetypes/Model.ini");
            auto obj = TTH::ObjectFactory::Create(file, name);
            obj->SetParent(TTH::ObjectManager::GetObject("SceneRoot"));
            obj->transform = newObjectTransform;
            obj->GetComponent<TTH::RenderComponent>()->SetModel(buf);
            newObjectTransform = {glm::vec3(0.0f), glm::vec3(0.0f), glm::vec3(1.0f), glm::vec3(0.0f)};
            TTH::ObjectManager::AddObject(obj);
            hierarchy.push_back(obj);
            ImGui::CloseCurrentPopup();
        }

        if(!ok){
            ImGui::PopStyleVar();
            ImGui::PopItemFlag();
        }

        ImGui::SetItemDefaultFocus();
        ImGui::SameLine();
        if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
        ImGui::EndPopup();
    }
}