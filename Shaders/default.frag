#version 330 core

out vec4 fragColor;

layout (std140) uniform Mirror{
    mat4 mirrorMatrix;
    vec3 mirrorPosition;
    vec3 mirrorNormal;
};

in vec3 FragPos;
in vec2 TexCoords;

uniform vec3 colorTint;

struct Light{
    vec3 position;
    vec3 color;
};

struct DirectionalLight{
    vec3 direction;
    vec3 color;
    mat4 lightSpaceMatrix;
};

in vec3 ViewPos;

in mat3 TBN;
uniform Light pointLights[32];
uniform int pointLights_count;

in DirectionalLight Sunlight;
uniform sampler2D sunlight_shadowMap;

in vec4 FragPosInLightSpace;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_normal1;

in vec3 FragPosNoTBN;

vec3 CalcLight(Light light, vec3 mat_normal){
    float ambient = 0.1;

    vec3 lightDir = normalize(TBN * light.position - FragPos);
    float diffuse = max(dot(lightDir, mat_normal), 0.0);

    vec3 viewDir = normalize(ViewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float specular = 0.5 * pow(max(dot(mat_normal, halfwayDir), 0.0), 32.0) * max(sign(diffuse), 0.0);

    vec3 lightIntensity = (ambient + diffuse + specular) * light.color;
    return lightIntensity;
}

vec3 CalcDirLight(DirectionalLight light, vec3 mat_normal){
    float ambient = 0.01;

    vec3 lightDir = normalize(-light.direction);
    float diffuse = max(dot(lightDir, mat_normal), 0.0);
    vec3 viewDir = normalize(ViewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float specular = 0.5 * pow(max(dot(mat_normal, halfwayDir), 0.0), 32.0) * max(sign(diffuse), 0.0);

    vec3 projCoords = FragPosInLightSpace.xyz / FragPosInLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float currentDepth = projCoords.z;

    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(sunlight_shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(sunlight_shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth > pcfDepth ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;

    if(projCoords.z > 1.0)
        shadow = 0.0;

    vec3 lightIntensity = (ambient + (diffuse + specular) * (1.0 - shadow)) * light.color;
    return lightIntensity;
}

void main() {

    if(dot(mirrorNormal, FragPosNoTBN - mirrorPosition) < 0.0){
        discard;
    }

    vec3 material_diffuse = texture( texture_diffuse1, TexCoords ).rgb * colorTint;

    vec3 material_normal = normalize(texture( texture_normal1, TexCoords ).rgb);
    material_normal = normalize(material_normal*2.0-1.0);

    vec3 lightIntensity = CalcDirLight(Sunlight, material_normal);
    for(int i = 0; i < pointLights_count; i++){
        lightIntensity += CalcLight(pointLights[i], material_normal);
    }
    vec3 outColor = lightIntensity * material_diffuse;

    fragColor = vec4(outColor, 1.0);
}
