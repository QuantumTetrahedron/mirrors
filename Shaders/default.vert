#version 330 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoords;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTangent;
layout(location = 4) in vec3 vBitangent;

uniform mat4 model;

layout (std140) uniform Matrices{
    mat4 projection;
    mat4 view;
    mat4 projectionView;
};

layout (std140) uniform Mirror{
    mat4 mirrorMatrix;
    vec3 mirrorPosition;
    vec3 mirrorNormal;
};

struct Light{
    vec3 position;
    vec3 color;
};

struct DirectionalLight{
    vec3 direction;
    vec3 color;
    mat4 lightSpaceMatrix;
};

uniform vec3 viewPos;
uniform DirectionalLight sunlight;

out vec3 FragPos;
out vec2 TexCoords;
out mat3 TBN;

out DirectionalLight Sunlight;

out vec3 FragPosNoTBN;
out vec3 ViewPos;
out vec4 FragPosInLightSpace;

void main() {
    FragPos = vec3(model * vec4(vPosition, 1.0));
    TexCoords = vTexCoords;
    FragPosNoTBN = FragPos;

    mat3 normalMatrix = transpose(inverse(mat3(model)));
    vec3 T = normalize(normalMatrix * vTangent);
    vec3 N = normalize(normalMatrix * vNormal);
    T = normalize(T - dot(T, N) * N);
    //vec3 B = cross(N, T);
    vec3 B = normalize(normalMatrix * vBitangent);

    if (dot(cross(N, T), B) < 0.0){
        T = T * -1.0;
    }

    FragPosInLightSpace = sunlight.lightSpaceMatrix * vec4(FragPos, 1.0);
    TBN = transpose(mat3(T,B,N));
    FragPos = TBN * FragPos;
    ViewPos = TBN * vec3(mirrorMatrix * vec4(viewPos,1.0));

    Sunlight.direction = TBN * sunlight.direction;
    Sunlight.color = sunlight.color;
    Sunlight.lightSpaceMatrix = sunlight.lightSpaceMatrix;

    gl_Position = projectionView * mirrorMatrix * model * vec4(vPosition, 1.0);
}
