#version 330 core

uniform vec3 colorTint;
//uniform vec3 viewPos;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_shininess1;
uniform sampler2D texture_normal1;

uniform sampler2D mirrorFramebufferTexture;

uniform int screenWidth;
uniform int screenHeight;

layout (std140) uniform Mirror{
    mat4 mirrorMatrix;
    vec3 mirrorPosition;
    vec3 mirrorNormal;
};
struct Light{
    vec3 position;
    vec3 color;
};

struct DirectionalLight{
    vec3 direction;
    vec3 color;
    mat4 lightSpaceMatrix;
};

in mat3 TBN;
uniform Light pointLights[32];
uniform int pointLights_count;

in vec3 FragPos;
//in vec3 Normal;
in vec2 TexCoords;

in vec3 ViewPos;
//in Light PointLight;
in DirectionalLight Sunlight;
uniform sampler2D sunlight_shadowMap;
in vec4 FragPosInLightSpace;

out vec4 fragColor;

in vec3 FragPosNoTBN;

vec3 CalcLight(Light light, vec3 mat_normal){
    float ambient = 0.1;

    vec3 lightDir = normalize(TBN * light.position - FragPos);
    float diffuse = max(dot(lightDir, mat_normal), 0.0);

    vec3 viewDir = normalize(ViewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float specular = 0.5 * pow(max(dot(mat_normal, halfwayDir), 0.0), 32.0) * max(sign(diffuse), 0.0);

    vec3 outColor = (ambient + diffuse + specular) * light.color;
    return outColor;
}

vec3 CalcDirLight(DirectionalLight light, vec3 mat_normal){
    float ambient = 0.01;

    vec3 lightDir = normalize(-light.direction);
    float diffuse = max(dot(lightDir, mat_normal), 0.0);
    vec3 viewDir = normalize(ViewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float specular = 0.5 * pow(max(dot(mat_normal, halfwayDir), 0.0), 32.0) * max(sign(diffuse), 0.0);


    vec3 projCoords = FragPosInLightSpace.xyz / FragPosInLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float currentDepth = projCoords.z;

    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(sunlight_shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(sunlight_shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth > pcfDepth ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;

    if(projCoords.z > 1.0)
    shadow = 0.0;

    vec3 outColor = (ambient + (diffuse + specular) * (1.0 - shadow)) * light.color;
    return outColor;
}

void main(){

    if(dot(mirrorNormal, FragPosNoTBN - mirrorPosition) < 0.0){
        discard;
    }

    vec3 material_diffuse = texture( texture_diffuse1, TexCoords ).rgb * colorTint;
    vec3 material_normal = normalize(texture( texture_normal1, TexCoords ).rgb);
    material_normal = normalize(material_normal*2.0-1.0);

    vec3 outColor = CalcDirLight(Sunlight, material_normal);
    for(int i=0;i< pointLights_count;i++){
        outColor += CalcLight(pointLights[i], material_normal);
    }
    outColor = outColor * material_diffuse;

    //vec3 result = colorTint * CalcLight(pointLight);
    //vec3 tex = vec3(texture(texture_diffuse1, TexCoords));
    vec4 reflect = texture(texture_shininess1, TexCoords);

    vec2 texCoords = gl_FragCoord.xy;
    texCoords.x /= screenWidth;
    texCoords.y /= screenHeight;

    float ref = reflect.x;
    if(ref < 0.3)
        fragColor = vec4(outColor, 1.0);
    else
        fragColor = vec4(material_diffuse * vec3(texture(mirrorFramebufferTexture, texCoords)), 1.0);
}
