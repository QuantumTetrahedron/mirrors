#version 330 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoords;
layout(location = 2) in vec3 vNormal;

uniform mat4 model;

layout (std140) uniform Matrices{
    mat4 projection;
    mat4 view;
    mat4 projectionView;
};

layout (std140) uniform Mirror{
    mat4 mirrorMatrix;
    vec3 mirrorPosition;
    vec3 mirrorNormal;
};

struct Light{
    vec3 position;
    vec3 color;
};

struct DirectionalLight{
    vec3 direction;
    vec3 color;
    mat4 lightSpaceMatrix;
};
uniform DirectionalLight sunlight;
uniform vec3 viewPos;

out vec3 ViewPos;
out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoords;
out vec4 FragPosInLightSpace;

void main() {
    FragPos = vec3(model * vec4(vPosition, 1.0));
    ViewPos = vec3(mirrorMatrix * vec4(viewPos,1.0f));
    TexCoords = vTexCoords;
    Normal = normalize(transpose(inverse(mat3(model))) * vNormal);
    FragPosInLightSpace = sunlight.lightSpaceMatrix * vec4(FragPos, 1.0);

    gl_Position = projectionView * mirrorMatrix * model * vec4(vPosition, 1.0);
}
