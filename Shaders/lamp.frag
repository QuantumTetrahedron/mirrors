#version 330 core

out vec4 fragColor;

uniform vec3 colorTint;

in vec3 FragPos;

layout (std140) uniform Mirror{
    mat4 mirrorMatrix;
    vec3 mirrorPosition;
    vec3 mirrorNormal;
};

void main(){
    if(dot(mirrorNormal, FragPos - mirrorPosition) < 0.0){
        discard;
    }
    fragColor = vec4(colorTint,1.0);
}