#version 330 core

out vec4 fragColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

layout (std140) uniform Mirror{
    mat4 mirrorMatrix;
    vec3 mirrorPosition;
    vec3 mirrorNormal;
};

uniform vec3 colorTint;
in vec3 ViewPos;
//uniform vec3 viewPos;

struct Light{
    vec3 position;
    vec3 color;
};

struct DirectionalLight{
    vec3 direction;
    vec3 color;
    mat4 lightSpaceMatrix;
};
uniform DirectionalLight sunlight;
uniform sampler2D sunlight_shadowMap;

in vec4 FragPosInLightSpace;
uniform Light pointLight;
//uniform Light pointLight2;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_normal1;

vec3 CalcLight(Light light){
    float ambient = 0.1;

    vec3 lightDir = normalize(light.position - FragPos);
    float diffuse = max(dot(lightDir, Normal), 0.0);

    //vec3 mirroredVp = vec3(mirrorMatrix * vec4(viewPos, 1.0));
    //vec3 viewDir = normalize(mirroredVp - FragPos);
    vec3 viewDir = normalize(ViewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float specular = 0.5 * pow(max(dot(Normal, halfwayDir), 0.0), 32.0) * max(sign(diffuse), 0.0);

    vec3 outColor = (ambient + diffuse + specular) * light.color;
    return outColor;
}

vec3 CalcDirLight(DirectionalLight light, vec3 normal){
    float ambient = 0.01;

    vec3 lightDir = normalize(-light.direction);
    float diffuse = max(dot(lightDir, normal), 0.0);
    //vec3 viewDir = normalize(viewPos - FragPos);
    vec3 viewDir = normalize(ViewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float specular = 0.5 * pow(max(dot(normal, halfwayDir), 0.0), 32.0) * max(sign(diffuse), 0.0);

    vec3 projCoords = FragPosInLightSpace.xyz / FragPosInLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    //float closestDepth = texture(sunlight_shadowMap, projCoords.xy).r;
    float currentDepth = projCoords.z;
    float bias = max(0.01 * (1.0 - dot(normal, lightDir)), 0.005);
    //float shadow = currentDepth - bias > closestDepth ? 1.0 : 0.0;

    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(sunlight_shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(sunlight_shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;

    if(projCoords.z > 1.0)
        shadow = 0.0;

    vec3 outColor = (ambient + (diffuse + specular) * (1.0 - shadow)) * light.color;
    return outColor;
}

void main() {

    if(dot(mirrorNormal, FragPos - mirrorPosition) < 0.0){
        discard;
    }

    vec3 material_diffuse = texture( texture_diffuse1, TexCoords ).rgb * colorTint;

    //vec3 outColor = CalcLight(pointLight);// + CalcLight(pointLight2);
    vec3 outColor = CalcDirLight(sunlight, Normal);// + CalcLight(pointLight);
    outColor = outColor * material_diffuse;

    fragColor = vec4(outColor, 1.0);
}
