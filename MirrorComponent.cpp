
#include <TTH/Core/ComponentFactory.h>
#include "MirrorComponent.h"
#include <TTH/Graphics/Gfx.h>
#include <glad/glad.h>
#include <TTH/Core/CameraComponent.h>
#include <TTH/Core/ObjectManager.h>

TTH::ComponentRegister<MirrorComponent> MirrorComponent::reg("MirrorComponent");

int MirrorComponent::maxDepth = 0;
unsigned int MirrorComponent::depth = 0;
std::vector<TTH::Framebuffer> MirrorComponent::reflection_framebuffers = std::vector<TTH::Framebuffer>(maxDepth);
std::vector<TTH::Framebuffer> MirrorComponent::refraction_framebuffers = std::vector<TTH::Framebuffer>(maxDepth);
bool MirrorComponent::initialized = false;
TTH::UniformBuffer* MirrorComponent::ubo;
TTH::CameraComponent* MirrorComponent::camera;

std::deque<MirrorComponent::MirrorData> MirrorComponent::mirrorStack;

MirrorComponent *MirrorComponent::Clone() const {
    return new MirrorComponent();
}

void MirrorComponent::Draw() {
    if(Options::mirrorDepth != maxDepth){
        SetMaxDepth(Options::mirrorDepth);
    }

    // don't show a mirror in its own reflection
    if(!mirrorStack.empty() && mirrorStack.back().mirror == this) {
        return;
    }

    MirrorData currentMirror;
    if(!mirrorStack.empty()) {
        currentMirror = mirrorStack.back();
    } else {
        currentMirror.position = glm::vec3(0.0f);
        currentMirror.normal = glm::vec3(0.0f);
        currentMirror.matrix = glm::mat4(1.0f);
        currentMirror.framebuffer = camera->framebuffer;//nullptr;
    }

    auto t = parent->GetWorldTransform();
    t.pivot *= t.scale;
    t.scale = glm::vec3(1.0f);

    // calc mirror normal in world space
    glm::vec3 normal = Normal;//model space
    glm::mat4 rotM = t.GetModelMatrix();
    normal = glm::vec3(rotM * glm::vec4(normal, 0.0f));

    bool drawReflection = true;
    bool drawRefraction = isOneWayMirror;
    bool flipNormal = false;

    if(depth <= 0) {
        drawReflection = false;
        drawRefraction = false;
    }

    glm::vec3 mPos = glm::vec3(currentMirror.matrix * glm::vec4(t.position, 1.0f)); // this mirror's position when mirrored on the current mirror
    glm::vec3 mNorm = glm::vec3(currentMirror.matrix * glm::vec4(normal, 0.0f)); // this mirror's normal vector after mirroring
    float dot = glm::dot(glm::normalize(mNorm), glm::normalize(camera->GetPosition() - mPos));

    if(dot <= 0){
        if(isOneWayMirror){
            flipNormal = true;
        } else {
            drawReflection = false;
        }
    }

    Rect worldSpaceRect = rect.Multiply(currentMirror.matrix * parent->GetWorldTransform().GetModelMatrix());

    bool rectOnCamera = Frustum::fromCamera(camera).Contains(worldSpaceRect);
    drawReflection = drawReflection && rectOnCamera;
    drawRefraction = drawRefraction && rectOnCamera;

    bool rectOnPreviousMirrors = std::all_of(mirrorStack.begin(), mirrorStack.end(), [worldSpaceRect](const MirrorData& md){
        return md.frustum.Contains(worldSpaceRect);
    });
    drawReflection = drawReflection && rectOnPreviousMirrors;
    drawRefraction = drawRefraction && rectOnPreviousMirrors;

    if(drawReflection){
        camera->Use(); // Use to set clear color
        reflection_framebuffers[depth-1].BindAsTarget(); // but override framebuffer

        MirrorData thisMirror;
        glm::mat4 M = getMirrorMatrix(normal);
        thisMirror.matrix = currentMirror.matrix * M;
        thisMirror.position = t.position;
        thisMirror.normal = flipNormal ? -normal : normal;
        thisMirror.mirror = this;
        thisMirror.framebuffer = &reflection_framebuffers[depth-1];

        Frustum frustum;
        frustum.CalcPlanes(camera, worldSpaceRect);
        thisMirror.frustum = frustum;

        depth = depth - 1;

        ubo->SetMat4(thisMirror.matrix, 0);
        ubo->SetVec3(thisMirror.position, sizeof(glm::mat4));
        ubo->SetVec3(thisMirror.normal, sizeof(glm::mat4)+sizeof(glm::vec4));
        mirrorStack.push_back(thisMirror);

        Options::sceneDraws++;
        TTH::Gfx::Clear();
        TTH::Gfx::DrawWorldSpace(camera);

        mirrorStack.pop_back();
        ubo->SetMat4(currentMirror.matrix, 0);
        ubo->SetVec3(currentMirror.position, sizeof(glm::mat4));
        ubo->SetVec3(currentMirror.normal, sizeof(glm::mat4)+sizeof(glm::vec4));

        depth = depth + 1;

    }

    if(drawRefraction){
        camera->Use();
        refraction_framebuffers[depth-1].BindAsTarget();

        MirrorData thisMirror;
        thisMirror.matrix = currentMirror.matrix;
        thisMirror.position = t.position;
        thisMirror.normal = flipNormal ? normal : -normal;
        thisMirror.mirror = this;
        thisMirror.framebuffer = &refraction_framebuffers[depth-1];

        Frustum frustum;
        frustum.CalcPlanes(camera, worldSpaceRect);
        thisMirror.frustum = frustum;

        depth = depth - 1;

        ubo->SetMat4(thisMirror.matrix, 0);
        ubo->SetVec3(thisMirror.position, sizeof(glm::mat4));
        ubo->SetVec3(thisMirror.normal, sizeof(glm::mat4)+sizeof(glm::vec4));
        mirrorStack.push_back(thisMirror);

        Options::sceneDraws++;
        TTH::Gfx::Clear();
        TTH::Gfx::DrawWorldSpace(camera);

        mirrorStack.pop_back();
        ubo->SetMat4(currentMirror.matrix, 0);
        ubo->SetVec3(currentMirror.position, sizeof(glm::mat4));
        ubo->SetVec3(currentMirror.normal, sizeof(glm::mat4)+sizeof(glm::vec4));

        depth = depth + 1;
    }

    if(drawReflection) {
        shader->Use();
        shader->SetInt("mirrorFramebufferTexture", 10);
        glActiveTexture(GL_TEXTURE10);
        reflection_framebuffers[depth - 1].getTexture()->Bind();
    }

    if(drawRefraction){
        shader->Use();
        shader->SetInt("mirrorFramebufferTexture2",11);
        glActiveTexture(GL_TEXTURE11);
        refraction_framebuffers[depth-1].getTexture()->Bind();
    }

    if(depth == maxDepth){
        if(TTH::Gfx::IsUsingFramebuffer()) {
            TTH::Gfx::UseFramebuffer(TTH::Gfx::GetUsedFramebuffer());
        } else {
            TTH::Gfx::UseNoFramebuffer();
        }
    } else {
        currentMirror.framebuffer->BindAsTarget();
    }

    shader->Use();

    int w, h;
    TTH::Gfx::GetResolution(&w, &h);
    shader->SetInt("screenWidth", w);
    shader->SetInt("screenHeight", h);

    shader->SetMat4("model", parent->GetWorldTransform().GetModelMatrix());
    shader->SetVec3("colorTint", colorTint);
    model->Draw(*shader);

    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE11);
    glBindTexture(GL_TEXTURE_2D, 0);
}

glm::mat4 MirrorComponent::getMirrorMatrix(glm::vec3 normal) {
    glm::vec3 position = parent->GetWorldTransform().position;

    float p = glm::dot(position, normal);

    glm::mat4 mat;

    mat[0][0] = 1-2*normal.x*normal.x;
    mat[0][1] = -2*normal.x*normal.y;
    mat[0][2] = -2*normal.x*normal.z;
    mat[0][3] = 0;
    mat[1][0] = -2*normal.x*normal.y;
    mat[1][1] = 1-2*normal.y*normal.y;
    mat[1][2] = -2*normal.y*normal.z;
    mat[1][3] = 0;
    mat[2][0] = -2*normal.x*normal.z;
    mat[2][1] = -2*normal.y*normal.z;
    mat[2][2] = 1-2*normal.z*normal.z;
    mat[2][3] = 0;
    mat[3][0] = 2*normal.x*p;
    mat[3][1] = 2*normal.y*p;
    mat[3][2] = 2*normal.z*p;
    mat[3][3] = 1;

    return mat;
}

bool MirrorComponent::LoadFromFile(TTH::IniFile &file) {
    file.SetToSection("MirrorComponent");

    std::string modelName, shaderName;

    file.RequireValue("model", modelName);
    model = TTH::ResourceManager::GetModel(modelName);

    file.RequireValue("shader", shaderName);
    shader = TTH::ResourceManager::GetShader(shaderName);

    file.GetValue("colorTint", colorTint, glm::vec3(1.0f));

    file.GetValue("normal", Normal, glm::vec3(1.0f,0.0f,0.0f));

    file.GetValue("isOneWayMirror", isOneWayMirror, false);

    file.GetValue("castsShadows", castsShadows, true);

    return true;
}

void MirrorComponent::OnLoad() {
    RenderComponent::OnLoad();

    rect.topLeft = glm::vec3(0.0f, 4.0f, 1.0f);
    rect.topRight = glm::vec3(0.0f, 4.0f, -1.0f);
    rect.bottomLeft = glm::vec3(0.0f,0.0f,1.0f);
    rect.bottomRight = glm::vec3(0.0f,0.0f,-1.0f);

    if(!initialized){
        initialized = true;
        SetMaxDepth(Options::mirrorDepth);

        camera = TTH::ObjectManager::GetObject("SceneCamera")->GetComponent<TTH::CameraComponent>();

        try {
            ubo = TTH::ResourceManager::GetUniformBuffer("Mirror");
        } catch(...) {
            TTH::ResourceManager::CreateUniformBuffer("Mirror", sizeof(glm::mat4) + 2 * sizeof(glm::vec4));
            ubo = TTH::ResourceManager::GetUniformBuffer("Mirror");
            ubo->SetMat4(glm::mat4(1.0f), 0);
            ubo->SetVec3(glm::vec3(0.0f), sizeof(glm::mat4));
            ubo->SetVec3(glm::vec3(0.0f), sizeof(glm::mat4) + sizeof(glm::vec4));
        }
    }
}

void MirrorComponent::SetMaxDepth(int new_depth) {
    maxDepth = new_depth;
    depth = maxDepth;
    reflection_framebuffers.clear();
    refraction_framebuffers.clear();
    reflection_framebuffers = std::vector<TTH::Framebuffer>(maxDepth);
    refraction_framebuffers = std::vector<TTH::Framebuffer>(maxDepth);

    int w, h;
    TTH::Gfx::GetResolution(&w, &h);
    for(TTH::Framebuffer& framebuffer : reflection_framebuffers){
        framebuffer.Generate(w, h, true);
    }
    for(TTH::Framebuffer& framebuffer : refraction_framebuffers){
        framebuffer.Generate(w, h,true);
    }
}