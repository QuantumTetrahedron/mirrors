
#ifndef MIRRORS_UTILITY_H
#define MIRRORS_UTILITY_H


#include <glm/vec3.hpp>
#include <glm/detail/type_mat.hpp>
#include <TTH/Core/CameraComponent.h>

struct Rect{
    glm::vec3 topLeft;
    glm::vec3 topRight;
    glm::vec3 bottomLeft;
    glm::vec3 bottomRight;
    Rect Multiply(glm::mat4 matrix);
};

struct Plane{
    glm::vec3 normal;
    float d;
    void CalcPlane(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2);
    bool RectInsidePlane(Rect rect) const;
};

struct Frustum{
    Plane planes[5];
    void CalcPlanes(TTH::CameraComponent* c, Rect rect);
    bool Contains(Rect rect) const;

    static Frustum fromCamera(TTH::CameraComponent* c);
};

struct Options {
    static int mirrorDepth;
    static bool controlsEnabled;
    static float exposure;
    static int sceneDraws;
    static void SetExposure(float newExposure);
};


#endif //MIRRORS_UTILITY_H
