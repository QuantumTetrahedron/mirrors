
#ifndef MIRRORS_POINTLIGHTSRENDERER_H
#define MIRRORS_POINTLIGHTSRENDERER_H

#include <TTH/Graphics/RenderComponent.h>
#include "PointLightsManager.h"

class PointLightsRenderer : public TTH::RenderComponent{

public:
    PointLightsRenderer *Clone() const override;

    void Draw() override;

protected:
    void OnLoad() override;

protected:
    bool LoadFromFile(TTH::IniFile &file) override;

private:

    PointLightsManager* lightsManager;
    static TTH::ComponentRegister<PointLightsRenderer> reg;
};


#endif //MIRRORS_POINTLIGHTSRENDERER_H
